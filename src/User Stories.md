# User Stories
## SmartFridge User:
S1.1. As a SmartFridge User, when I’m inside the grocery, I want to get information about my fridge containment in order not to waste money on products which I already have.

S1.2. As a SmartFridge User, when I visit a shop after my work, I want to be notified about products which are out of stock in my fridge in order to remember to buy them.

S1.3. As a SmartFridge User, when I want to make a dish, I want to receive cooking recipes based on products that I have in order to quickly cook something and not waste time on finding appropriate recipes in Web.

S1.4. As a SmartFridge User, when I have lots of products inside the fridge, I want to be able to quickly check the condition of the products concerning its expiry date in order to consume or waste them as soon as possible and not to give someone a possibility to eat spoiled products.

S1.5. As a SmartFridge User, when I want to add products to my virtual fridge, I want to scan the QR-code of the receipt by phone camera and instantly add all the products from the receipt.

S1.6. As a SmartFridge User, when I want to remove a product from my virtual fridge, I want to choose the product in list of my products and remove it in one click.

## SmartFridge Admin:
S2.1. As a SmartFridge Admin, when I want to add recipe, I want to open the form of recipe creation, choose needed products from the list, write steps for cooking, select complexity of the recipe and choose appropriate name, and after that submit new recipe to system.

S2.2. As a SmartFridge Admin, when I want to select avg shelf life of a product type, I want to select a product type from list and select appropriate duration in days for the shelf life, after which all the durations of existing products should be updated according to my shelf life choice.

